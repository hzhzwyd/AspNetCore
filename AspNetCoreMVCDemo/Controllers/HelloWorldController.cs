using System.IO;
using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;

namespace AspNetCoreMVCDemo.Controllers
{
    public class HelloWorldController : Controller
    {
        
        // public string Index(){

        //     return "This is My Default Action ...";
        // }

        public IActionResult Index(){
            return View();
        }

        public IActionResult Welcome(string name ,int numTimes = 1){

            ViewData["Message"] = $"Hello {name}";
            ViewData["NumTimes"] =  numTimes;

            return View();
            //return  HtmlEncoder.Default.Encode($"Hello {name}, NumTimes is:{numTimes}");
            //return "This is the Welcome action method...";
        }
        
    }
}