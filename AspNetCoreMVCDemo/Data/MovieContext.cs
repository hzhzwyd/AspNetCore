using AspNetCoreMVCDemo.Models;
using Microsoft.EntityFrameworkCore;

namespace AspNetCoreMVCDemo.Data
{
    public class MovieContext : DbContext
    {

        public MovieContext(DbContextOptions<MovieContext> options):base(options)
        {

        }

        public DbSet<Movie> Movie{get;set;}
        
    }
}