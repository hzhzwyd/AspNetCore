using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RazorPageWebDemo1.Models;

namespace RazorPageWebDemo1.Data {
    public class MovieContext : DbContext {
        public MovieContext (DbContextOptions<MovieContext> options) : base (options) { }

        public DbSet<RazorPageWebDemo1.Models.Movie> Movie { get; set; }
    }
}