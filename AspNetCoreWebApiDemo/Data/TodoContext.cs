using AspNetCoreWebApiDemo.Models;
using Microsoft.EntityFrameworkCore;

namespace AspNetCoreWebApiDemo.Data {
    public class TodoContext : DbContext {
        public TodoContext (DbContextOptions<TodoContext> options) : base (options) {

        }

        public DbSet<TodoItem> TodoItems { get; set; }

    }
}